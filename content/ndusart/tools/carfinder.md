+++
title="Car Finder"
date="2016-03-20T20:45:36+01:00"
lastmod="2016-08-11T22:24:50+01:00"
github="https://github.com/ndusart/carfinder"
description="Android application that helps people find their car in the concrete jungle."
platforms=["android"]
+++

Car Finder is an Android application that helps you find where you parked your car.

Unlike other applications of this kind, this one detects when you leave your car and save the current position automatically. In other apps you generally have to push a button to tell the app you just parked. That removes quite all the interest of that solution as if you're likely to forget where you parked your car (like me...), then you'll probably forget to open your app each time you leave your car as well. 

### Requirements

 + **Car with a bluetooth handsfree system:**
   There is no magic.
   The app detects when you loose the Bluetooth connection with your car to determine that you just turned the engine off and are about to leave the car.

 + **Location capable smartphone:**
   Well without that, the app cannot locates you.
   A data plan can be required if you have no GPS (or do not want to use it) as Android can use this network to locate you.

{{< playstore "be.ndusart.carfinderapp" >}}
