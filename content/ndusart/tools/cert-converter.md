+++
title="Certificate Converter"
date="2016-07-20T21:30:50+01:00"
Lastmod="2016-07-20T21:30:50+01:00"
description="Convertion tool for X509 certificates and RSA keys. It allows to convert between DER and PEM formats."
loadinglayer="manual"
scripts=["/js/openssl-wrapper.js","/js/converter.js"]
asyncscripts=["/js/openssl.js"]
needjs="true"
platforms=["web"]
+++

Select a X509 certificate or a RSA key file in DER or PEM format, to convert it to the other format.

{{< file-uploader "uploader" "files" >}}

{{< named-ul "files" >}}

{{< named-div "powered_by_openssl" >}}
