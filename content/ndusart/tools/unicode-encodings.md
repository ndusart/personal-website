+++
title="Unicode Encodings Tool"
date="2016-03-18T20:44:20+01:00"
lastmod="2016-03-18T20:44:20+01:00"
description="Tool to convert a character in its different Unicode representations (UTF-8, UTF-16 or UTF-32)."
loadinglayer="auto"
scripts=["/js/unicodecharacter.js","/js/unicode-encodings-ui.js"]
ondocready="initUI"
needjs="true"
platforms=["web"]
+++

Enter a character in this input field

{{< named-input "unicode-input" "text" >}} {{< named-select format character utf8 utf16 utf32 codepoint >}} {{< warning format-error >}}{{< /warning >}}

UTF-8: {{< named-span "utf8" >}}

UTF-16: {{< named-span "utf16" >}}

UTF-32: {{< named-span "utf32" >}}

Xml Entity: {{< named-span "xml-entity" >}}

Character: {{< named-span "character" >}}

Code point: {{< named-span "codepoint" >}}

{{< named-span "downloading" >}}
