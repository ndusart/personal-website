
function fade(element) {
	var op = 1;  // initial opacity
	var timer = setInterval(function () {
		if (op < 0){
			clearInterval(timer);
			element.style.display = 'none';
		}
		element.style.opacity = op;
		op -= 0.04;
	}, 5);
}
