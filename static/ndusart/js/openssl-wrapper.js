
var fct_version;
var fct_md5;
var fct_hexstring;
var fct_check_x509_der;
var fct_check_x509_pem;
var fct_check_pkey_der;
var fct_check_pkey_pem;
var fct_x509_der2pem;
var fct_x509_pem2der;
var fct_pkey_der2pem;
var fct_pkey_pem2der;

function initWrapper()
{
	fct_version =  Module.cwrap('opensslversion', 'string', []);
	fct_md5 = Module.cwrap('jsmd5', 'number', ['number', 'number']);
	fct_hexstring =  Module.cwrap('hexstring', 'string', ['number', 'number']);
	fct_x509_der2pem = Module.cwrap('x509_der_to_pem', 'number', ['number', 'number', 'number']);
	fct_x509_pem2der = Module.cwrap('x509_pem_to_der', 'number', ['number', 'number', 'number']);
	fct_pkey_der2pem = Module.cwrap('pkey_der_to_pem', 'number', ['number', 'number', 'number']);
	fct_pkey_pem2der = Module.cwrap('pkey_pem_to_der', 'number', ['number', 'number', 'number']);
	fct_check_x509_der = Module.cwrap('check_x509_der', 'number', ['number', 'number']);
	fct_check_x509_pem = Module.cwrap('check_x509_pem', 'number', ['number', 'number']);
	fct_check_pkey_der = Module.cwrap('check_pkey_der', 'number', ['number', 'number']);
	fct_check_pkey_pem = Module.cwrap('check_pkey_pem', 'number', ['number', 'number']);
}

function getOpenSSLVersion()
{
	return fct_version();
}

function md5(str)
{
	var strptr = Module._malloc(str.length);
	Module.writeAsciiToMemory(str, strptr);
	
	var resultptr = fct_md5(strptr, str.length);
	
	var md5value = fct_hexstring(resultptr, 16);
	
	Module._free(resultptr);
	Module._free(strptr);
	
	return md5value;
}

function check_is_x509_DER(data)
{
	var dataptr = Module._malloc(data.length);
	for(i=0; i<data.length; ++i)
	{
		Module.setValue(dataptr + i,data[i],'i8');
	}
	
	var result = (fct_check_x509_der(dataptr, data.length) != 0);
	
	Module._free(dataptr);
	
	return result;
}

function check_is_x509_PEM(data)
{
	var dataptr = Module._malloc(data.length);
	for(i=0; i<data.length; ++i)
	{
		Module.setValue(dataptr + i,data[i],'i8');
	}
	
	var result = (fct_check_x509_pem(dataptr, data.length) != 0);
	
	Module._free(dataptr);
	
	return result;
}

function check_is_pkey_DER(data)
{
	var dataptr = Module._malloc(data.length);
	for(i=0; i<data.length; ++i)
	{
		Module.setValue(dataptr + i,data[i],'i8');
	}
	
	var result = (fct_check_pkey_der(dataptr, data.length) != 0);
	
	Module._free(dataptr);
	
	return result;
}

function check_is_pkey_PEM(data)
{
	var dataptr = Module._malloc(data.length);
	for(i=0; i<data.length; ++i)
	{
		Module.setValue(dataptr + i,data[i],'i8');
	}
	
	var result = (fct_check_pkey_pem(dataptr, data.length) != 0);
	
	Module._free(dataptr);
	
	return result;
}

function x509_DER_to_PEM(data)
{
	var dataptr = Module._malloc(data.length);
	for(i=0; i<data.length; ++i)
	{
		Module.setValue(dataptr + i,data[i],'i8');
	}
	
	var outlenptr = Module._malloc(4);
	
	var resultptr = fct_x509_der2pem(dataptr, data.length, outlenptr);
	
	var outlen = Module.getValue(outlenptr,'i32');
	Module._free(outlenptr);
	
	var array = [];

	for(i=0; i<outlen; ++i)
	{
		var value = Module.getValue(resultptr+i,'i8');
		array.push(value);
	}
	
	Module._free(dataptr);
	
	return new Uint8Array(array);
}

function x509_PEM_to_DER(data)
{
	var dataptr = Module._malloc(data.length);
	for(i=0; i<data.length; ++i)
	{
		Module.setValue(dataptr + i,data[i],'i8');
	}
	
	var outlenptr = Module._malloc(4);
	
	var resultptr = fct_x509_pem2der(dataptr, data.length, outlenptr);
	
	var outlen = Module.getValue(outlenptr,'i32');
	Module._free(outlenptr);
	
	var array = [];

	for(i=0; i<outlen; ++i)
	{
		var value = Module.getValue(resultptr+i,'i8');
		array.push(value);
	}
	
	Module._free(dataptr);
	
	return new Uint8Array(array);
}

function pkey_PEM_to_DER(data)
{
	var dataptr = Module._malloc(data.length);
	for(i=0; i<data.length; ++i)
	{
		Module.setValue(dataptr + i,data[i],'i8');
	}
	
	var outlenptr = Module._malloc(4);
	
	var resultptr = fct_pkey_pem2der(dataptr, data.length, outlenptr);
	
	var outlen = Module.getValue(outlenptr,'i32');
	Module._free(outlenptr);
	
	var array = [];

	for(i=0; i<outlen; ++i)
	{
		var value = Module.getValue(resultptr+i,'i8');
		array.push(value);
	}
	
	Module._free(dataptr);
	
	return new Uint8Array(array);
}

function pkey_DER_to_PEM(data)
{
	var dataptr = Module._malloc(data.length);
	for(i=0; i<data.length; ++i)
	{
		Module.setValue(dataptr + i,data[i],'i8');
	}
	
	var outlenptr = Module._malloc(4);
	
	var resultptr = fct_pkey_der2pem(dataptr, data.length, outlenptr);
	
	var outlen = Module.getValue(outlenptr,'i32');
	Module._free(outlenptr);
	
	var array = [];

	for(i=0; i<outlen; ++i)
	{
		var value = Module.getValue(resultptr+i,'i8');
		array.push(value);
	}
	
	Module._free(dataptr);
	
	return new Uint8Array(array);
}
