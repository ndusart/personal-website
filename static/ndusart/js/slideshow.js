
function Slideshow(element, imagesSrc, period) {
	this.images = imagesSrc;
	this.imgIndex = 0;
	this.period = period;

	this.div = element;
	var mainImg = document.createElement("img");
	mainImg.style.zIndex = "1";
	mainImg.style.opacity = "1";
	mainImg.style.position = "static"; // main img is static so that it takes place
	mainImg.src = this.images[0];
	this.img = mainImg;

	var subImg = document.createElement("img");
	subImg.style.zIndex = "-1";
	subImg.style.opacity = "0";
	subImg.style.position = "absolute";
	subImg.style.top = "0";
	subImg.style.left = "0";
	subImg.src = this.images[1];
	this.subImg = subImg;

	element.appendChild(this.img);
	element.appendChild(this.subImg);
}

Slideshow.prototype.start = function(){
	this.fadeToNextImage();

	var slideshow = this;
	setInterval(function(){slideshow.fadeToNextImage();}, this.period);
}

Slideshow.prototype.fadeToNextImage = function() {
	// flip the elements
	var sub = this.subImg;
	this.subImg = this.img;
	this.img = sub;
	this.subImg.style.zIndex = "-1";
	this.img.style.zIndex = "1";

	// fade in the new image
	this.subImg.classList.add("fade-out");
	this.img.classList.add("fade-in");

	var slideshow = this;
	setTimeout(function(){slideshow.prepareNextImage();}, 1500);
}

Slideshow.prototype.prepareNextImage = function() {
	this.imgIndex = this.imgIndex + 1;
	if( this.imgIndex >= this.images.length )
		this.imgIndex = 0;
	this.subImg.src = this.images[this.imgIndex];

	this.img.style.opacity = "1";
	this.subImg.style.opacity = "0";

	this.subImg.className = "";
	this.img.className = "";
}