
function initUI() {
  document.getElementById("unicode-input").oninput = onInputChange;
  document.getElementById("format").onchange = onInputChange;

  if( localStorage.getItem("done") === null ) {
    document.getElementById("downloading").innerHTML = "Downloading character names ...";
    setTimeout(downloadUnicodeNames, 5000);
  }
}

function onInputChange() {
  var character = document.getElementById("unicode-input").value;
  resetError();

  if( character.length === 0 ) {
    emptyAll();
  } else {
    var format = document.getElementById("format").value;

    if( correctFormat(character, format) ) {
      var unicode = new UnicodeCharacter(character, format);
      document.getElementById("utf8").innerHTML = unicode.toUTF8();
      document.getElementById("utf16").innerHTML = unicode.toUTF16();
      document.getElementById("utf32").innerHTML = unicode.toUTF32();
      document.getElementById("xml-entity").innerHTML = encodeStringInHtmlEntities(unicode.toXmlEntity());
      document.getElementById("character").innerHTML = unicode.toCharacter();
      document.getElementById("codepoint").innerHTML = unicode.getCodePoint(true);

      var name = localStorage.getItem(unicode.getCodePoint().toString());
      if( name !== null ) {
        document.getElementById("character").innerHTML += " - " + name;
      }
    } else {
      emptyAll();
    }
  }
}

function emptyAll() {
  document.getElementById("utf8").innerHTML = "";
  document.getElementById("utf16").innerHTML = "";
  document.getElementById("utf32").innerHTML = "";
  document.getElementById("xml-entity").innerHTML = "";
  document.getElementById("character").innerHTML = "";
  document.getElementById("codepoint").innerHTML = "";
}

function encodeStringInHtmlEntities(str) {
  return str.replace(/[\u00A0-\u9999<>\&]/gim, function(i) {
    return '&#'+i.charCodeAt(0)+';';
  });
}

function correctFormat(character, format) {
  var ok = true;

  if( format == "character" ) {
    if( character.length !== 1) {
        if( character.length === 2 && (new UnicodeCharacter(character)).toUTF16().length > 4 ) {
          ok = true;
        } else {
          ok = false;
          setError("You should write only one character");
        }
    }
  } else if( format == "utf8" || format == "utf16" || format == "utf32" ) {
    if (character.search(/^[a-fA-F0-9 ]+$/g) === -1 ) {
      ok = false;
      setError("You should only enter hexadecimal digits (0-9, a-f and A-F)");
    }
  }

  return ok;
}

function setError(error) {
  document.getElementById("format-error").innerHTML = error;
}

function resetError() {
  document.getElementById("format-error").innerHTML = "";
}

function downloadUnicodeNames() {
  var req = new XMLHttpRequest();
  req.open('GET', '/raw/UnicodeData.txt', true);
  req.onreadystatechange = function (aEvt) {
    if (req.readyState == 4) {
       if(req.status == 200)
        var data = req.responseText;
        var lines = data.split("\n");
        lines.forEach(function( line, index, arr) {
          var elements = line.split(";");
          if( elements.length <= 1 )
            return;

          var point = elements[0];
          var name = elements[1];
          localStorage.setItem(point, name);
        });
        localStorage.setItem("done", "true");
        document.getElementById("downloading").innerHTML = "";
    }
  };
  req.send(null);
}
