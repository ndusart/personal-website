function allReady()
{
	initWrapper();
	removeLoading();
	
	document.getElementById('powered_by_openssl').innerHTML = "Powered by " + getOpenSSLVersion();
	
	document.getElementById('uploader').addEventListener('change', handleFileSelect, false);
	
	document.getElementById('uploader').click();
}

function fade(element) {
	var op = 1;  // initial opacity
	var timer = setInterval(function () {
		if (op < 0){
			clearInterval(timer);
			element.style.display = 'none';
		}
		element.style.opacity = op;
		op -= 0.04;
	}, 5);
}

function removeLoading()
{
	fade(document.getElementById('loadinglayer'));
}

function handleFileSelect(evt) {
	var files = evt.target.files; // FileList object

	// files is a FileList of File objects. List some properties.
	var output = [];
	for (var i = 0, f; f = files[i]; i++) {
		var reader = new FileReader();
		
		reader.onload = (function(theFile) {
			return function(e) {
				var ul = document.getElementById("files");
				var li = document.createElement("li");
				
				var filename = theFile.name;
				var extindex = filename.lastIndexOf(".");
				
				if( extindex > 0 )
					filename = filename.substring(0, extindex);
				
				li.innerHTML = filename;
				
				var array = new Uint8Array(e.target.result);
				if( check_is_x509_DER(array) )
					new_x509_der(li, filename, array);
				else if( check_is_x509_PEM(array) )
					new_x509_pem(li, filename, array);
				else if( check_is_pkey_PEM(array) )
					new_pkey_pem(li, filename, array);
				else if( check_is_pkey_DER(array) )
					new_pkey_der(li, filename, array);
				else
					li.innerHTML = li.innerHTML + ' (unknown type)';
				
				ul.appendChild(li);
			};
		})(f);
		
		reader.readAsArrayBuffer(f);
	}
}

function new_x509_der(li, filename, array)
{
	li.innerHTML = li.innerHTML + ' (x509 certificate)';
	var pem = x509_DER_to_PEM(array);
	var pemblob = new Blob([pem], {type: "application/octet-stream"});
	
	var derblob = new Blob([array], {type: "application/octet-stream"});
	
	li.innerHTML = li.innerHTML + ': <a href="' + window.URL.createObjectURL(pemblob) + '" download="' + filename + '.pem">PEM</a> / <a href="' + window.URL.createObjectURL(derblob) + '" download="' + filename + '.cer">DER</a>';
}

function new_x509_pem(li, filename, array)
{
	li.innerHTML = li.innerHTML + ' (x509 certificate)';
	var der = x509_PEM_to_DER(array);
	var derblob = new Blob([der], {type: "application/octet-stream"});
	
	var pemblob = new Blob([array], {type: "application/octet-stream"});
	
	li.innerHTML = li.innerHTML + ': <a href="' + window.URL.createObjectURL(pemblob) + '" download="' + filename + '.pem">PEM</a> / <a href="' + window.URL.createObjectURL(derblob) + '" download="' + filename + '.cer">DER</a>';
}

function new_pkey_pem(li, filename, array)
{
	li.innerHTML = li.innerHTML + ' (Private key)';
	var der = pkey_PEM_to_DER(array);
	var derblob = new Blob([der], {type: "application/octet-stream"});
	
	var pemblob = new Blob([array], {type: "application/octet-stream"});
	
	li.innerHTML = li.innerHTML + ': <a href="' + window.URL.createObjectURL(pemblob) + '" download="' + filename + '.pem">PEM</a> / <a href="' + window.URL.createObjectURL(derblob) + '" download="' + filename + '.key">DER</a>';
}

function new_pkey_der(li, filename, array)
{
	li.innerHTML = li.innerHTML + ' (Private key)';
	var pem = pkey_DER_to_PEM(array);
	var pemblob = new Blob([pem], {type: "application/octet-stream"});
	
	var derblob = new Blob([array], {type: "application/octet-stream"});
	
	li.innerHTML = li.innerHTML + ': <a href="' + window.URL.createObjectURL(pemblob) + '" download="' + filename + '.pem">PEM</a> / <a href="' + window.URL.createObjectURL(derblob) + '" download="' + filename + '.key">DER</a>';
}
