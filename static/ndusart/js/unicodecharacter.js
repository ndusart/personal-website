
function UnicodeCharacter(value, valueType) {
	var codePoint = 0;

	if( value === undefined )
		return;

	if( typeof value == 'number' )
		this.setCodePoint(value);
	else if( typeof value == 'string' ) {
		if( valueType === undefined )
			this.setCharacter(value);
		else if( valueType == 'utf8' )
			this.setUTF8(value);
		else if( valueType == 'utf16' )
			this.setUTF16(value);
		else if( valueType == 'codepoint' )
			this.setCodePoint(value);
		else
			this.setCharacter(value);
	}
}

UnicodeCharacter.prototype.toUTF8 = function() {
	var value = this.codePoint;

	if( value < 0 )
		return "(invalid)";

	var bits_available = 7; // bits available in first byte
	var result = '';

	while( true ) {
		var firstMask = (1 << bits_available) - 1;
		var header = 0;
		if( value <= firstMask ) {
			header = (0xFF - firstMask - (1<<bits_available));
			result = (header+(firstMask&value)).toString(16) + ' ' + result;
			value >>= bits_available;
			break;
		} else {
			var mask = 0b00111111;
			bits_available--;
			if( bits_available > 5)
				bits_available = 5;
			else if( bits_available <= 0 )
				break;
			header = 0b10000000;
			result = (header+(value&mask)).toString(16) + ' ' + result;
			value >>= 6;
		}
	}

	return result.trim().toUpperCase();
};

UnicodeCharacter.prototype.toUTF16 = function(le) {
	if( this.codePoint < 0 )
		return "(invalid)";

	var result = '';
	if( this.codePoint <= 0xFFFF )
		result = (this.codePoint<4096?'0':'')+(this.codePoint<256?'0':'')+this.codePoint.toString(16).toUpperCase();
	else if( this.codePoint <= 0x10FFFD ) {
		var y = this.codePoint & 0x000003FF;
		var x = (this.codePoint & 0x0000FC00) >> 10;
		var w = ((this.codePoint & 0xFFFF0000) >> 16) -1;
		var value = 0x100000000 + y + (0b110111 << 10) + (x << 16) + (w << 22) + (0b110110 << 26);
		result = value.toString(16).toUpperCase();
	}

	if( le ) {
		var leresult = '';
		for(var i = 0; i<result.length; i+=4) {
			var word = result.substring(i, i+4);
			leresult = leresult + word.substring(2,4) + word.substring(0,2);
		}
		result = leresult;
	}

	if( result.length > 4 )
		result = result.substring(0,4) + ' ' + result.substring(4, 8);

	return result;
};

UnicodeCharacter.prototype.toUTF32 = function(le) {
	if( this.codePoint < 0 )
		return "(invalid)";

	var result = this.codePoint.toString(16).toUpperCase();
	while(result.length < 8 )
		result = '0' + result;

	if( le ) {
		var leresult = '';
		for(var i = 0; i<result.length; i+=8) {
			var word = result.substring(i, i+8);
			leresult = leresult + word.substring(6,8) + word.substring(4,6) + word.substring(2,4) + word.substring(0,2);
		}
		result = leresult;
	}

	return result;
};

UnicodeCharacter.prototype.toCharacter = function() {
	if( this.codePoint < 0 )
		return "(invalid)";

	return String.fromCodePoint(this.codePoint);
};

UnicodeCharacter.prototype.getCodePoint = function(unicodeString) {
	if( this.codePoint < 0 )
		return "(invalid)";

	return ((unicodeString?'U+':'')+(this.codePoint<4096?'0':'')+(this.codePoint<256?'0':'')+this.codePoint.toString(16)).toUpperCase();
};

UnicodeCharacter.prototype.setUTF8 = function(value) {
	function removeSignificantOnes(byte) {
		var count = 0;
		while( byte >= 0x80 ) {
			count++;
			byte = (byte << 1) & 0xFF;
		}
		byte >>= count;
		return [byte, 7-count];
	}

	value = value.replace(/[^0-9a-fA-F]/gi, '');
	if( (value.length % 2) !== 0 )
		value = '0' + value;
	var bytes = value.match(/.{2}/g);

	this.codePoint = 0;
	var self = this;

	bytes.forEach(function(byte, index, arr){
		var b = parseInt(byte, 16);
		var bits = removeSignificantOnes(b);
		self.codePoint <<= bits[1];
		self.codePoint += bits[0];
	});

	var check = this.toUTF8().toLowerCase();
	check = check.replace(/[^0-9a-f]/g, '');
	if( check != value.toLowerCase() )
		this.codePoint = -1;
};

UnicodeCharacter.prototype.setUTF16 = function(value) {
	// TODO
};

UnicodeCharacter.prototype.setUTF32 = function(value) {
	// TODO
};

UnicodeCharacter.prototype.setCodePoint = function(value) {
	this.codePoint = parseInt(value.replace(/[^0-9a-fA-F]/gi, ''), 16);
};

UnicodeCharacter.prototype.setCharacter = function(character) {
	this.codePoint = character.codePointAt(0);
};

UnicodeCharacter.prototype.toXmlEntity = function(base) {
	if( this.codePoint < 0 )
		return "(invalid)";
	
	if( base === undefined )
		base = 16;
	var val = '';
	switch(base){
		case 10:
			val = '&#' + this.codePoint.toString() + ';';
			break;
		case 16:
			val = '&#x' + this.codePoint.toString(16).toUpperCase() + ';';
			break;
		default:
			break;
	}
	return val;
};
